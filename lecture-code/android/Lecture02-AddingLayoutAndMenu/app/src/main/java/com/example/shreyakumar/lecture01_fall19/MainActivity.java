package com.example.shreyakumar.lecture01_fall19;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.*;
import android.view.View;
import android.view.Menu;
import org.w3c.dom.Text;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private TextView mSearchResultsTextView; // class property for other methods to also access it later
    private TextView mDisplayTextView;
    private EditText mSearchBoxEditText;
    private Button   mSearchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // R class which allows access to resources through ids and such.

        mSearchResultsTextView = (TextView) findViewById(R.id.tv_results);
        mDisplayTextView       = (TextView) findViewById(R.id.tv_display_text);
        mSearchBoxEditText     = (EditText) findViewById(R.id.et_search_box);
        mSearchButton          = (Button)   findViewById(R.id.button_search);

        System.out.println("DEBUG: @@@@@@@@@@@@@@@@@@@@@@@@@@@" + mSearchResultsTextView.getText());

        mSearchResultsTextView.append("\n\nHermes \n\n\nIrene\n\n\nJohn");

        String[] studentNames = {"Timothy", "Kathryn", "Maria", "Thomas", "Auna", "Alex", "Aemile", "Hind", "Name1", "Name2", "Name3", "Name4", "Name5"};

        for(String name : studentNames){
            mSearchResultsTextView.append("\n\n" + name);
        } // end of for

        // detecting and responding to button click.
        mSearchButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v){ // onClick() is an inner method definition. CLOSURE
                        // this code is executed when the button is clicked.
                        String searchText = mSearchBoxEditText.getText().toString();
                        Log.d("informational", "Search Button Clicked!" + searchText);
                        String msg = "Searching for " + searchText;
                        mSearchResultsTextView.setText(msg);
                        Context c = MainActivity.this;
                        Toast.makeText(c, msg, Toast.LENGTH_LONG).show(); // makeText - static method

                    } // end of inner method - onClick()
                } // end of object View.OnClickListener
        ); // end of setOnClickListener

    } // end of onCreate.

    // menu inflation and action code
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    } // end

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int menuItemSelected = item.getItemId();
        if(menuItemSelected == R.id.action_about){ // TODO
            String msg = "About menu item clicked!";
            Log.d("informational", msg);
            Context c = MainActivity.this;
            Toast.makeText(c, msg, Toast.LENGTH_LONG).show(); // makeText - static method
        }
        return true;
    }

} // end of class.
