// draft non-working ZooTest structure

public class ZooTest{

    // property
    public Scanner sc = new Scanner(System.in); // local to ZooTest, accessible to methods.

    public static Animal [] setupAnimals(){
        Animal [] animals = new Animal[10];
        animals[0] = new Dog("Bagheera");
        animals[1] = new Whale("Willy");

        return animals;
    } // end of setupAnimals

    public static void printSummaryView(Animal [] animals){
        // walk through animals array, count each species as you are walking through it.
        int countWhale = 0;
        int countDog = 0;

        for(int i=0; i< animals.length; i++){
            if(animals[i] instanceof Whale){
                countWhale++;
            }

        }
        // print the summary of species.
    } // end of printSummaryView

    public static void printVerboseList(Animal [] animals){
        for(int i=0; i< animals.length; i++){
            //System.out.println(animals[i]); // this only works if toString() is defined for these objects
            System.out.println((i+1) + ": " animals[i].name + "  " + animals[i].getClass().getSimpleName());
        }
    } // end of printVerboseList

    public static Animal [] checkAnimalInput(Animal [] alist){
        // check the user's input
        // if add, then create an animal and call addAnimal
        Animal a; // reference type Animal
        String input = sc.next();
        input = input.toLowerCase();
        // create species based object using new and checking user input
        if(input.equals("Dog")){
            String animalName = input.next();
            a = new Dog(animalName); // here we bind 'a' to Object type Dog.
            // do not say Dog d = new Dog();
        }

        alist = addAnimal(a, alist);

    } // end of checkAnimalInput

    Animal [] addAnimal(Animal a, Animal [] alist){

        // adding a to alist, possibly resizing
        return alist; // updated alist

    }

    public static void main(String[] args) {

        // setup initial animals
        Animal [] alist = setupAnimals();

        // print summary view
        printSummaryView(alist);

        // print Verbose list
        printVerboseList(alist);

        // interactive add/delete/display
        String input = "";
        while(!input.toLowerCase().equals("exit")){
            input = sc.nextLine(); // next(); hasNext()

            alist = checkAnimalInput(alist); // grab the updated animal list.
        }



    } // end of main method

}
